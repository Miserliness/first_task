import csv
import hashlib
import ipinfo
from geopy.geocoders import Nominatim
import xlsxwriter

#--Парсим csv файл по заголовкам в словарь
csv_results = []
with open('csv/test_data.csv') as csvfile:
    reader = csv.DictReader(csvfile, delimiter=';')
    for row in reader:
        csv_results.append(row)

#--Парсим токен для использования сервиса ipinfo
#--с помощью библиотеки ipinfo
access_token = ''
with open('csv/ipinfo_token.csv') as csvfile:
    reader = csv.DictReader(csvfile)
    for row in reader:
        access_token = row['access_token']


#--Расчитываем хэш SHA-256 для каждого ip из csv файла
ip_sha256 = []
for res in csv_results:
    ip_addr = res['ip_address']
    hashvalue=hashlib.sha256(ip_addr.encode())
    ip_sha256.append(hashvalue.hexdigest())


#--Определяем страну, город, координаты по ip-адресу
#--с помощью библиотеки ipinfo
country_city_loc_by_ip = []
handler = ipinfo.getHandler(access_token)
for res in csv_results:
    ip_addr = res['ip_address']
    details = handler.getDetails(ip_addr)
    country_city_loc_by_ip.append((details.country_name, details.city, details.loc))
    
#--Определяем страну, город по координатам
#--с помощью библиотеки geopy
country_city_by_coords = []
geolocator = Nominatim(user_agent="geoapiExercises")
for res in csv_results:
    location = geolocator.reverse(res['lat']+","+res['lon'], language='en')
    address = location.raw['address']
    country_city_by_coords.append((address.get('country', ''), (address.get('city', ''))))

#--Определяем координаты по названию города
coords_by_city = []
for res in csv_results:
    location = geolocator.geocode(res['city'])
    coords_by_city.append((location.latitude, location.longitude))


#--Создаем файл Exel для результатов скрипта и записываем реузльтат
workbook = xlsxwriter.Workbook('results.xlsx')
worksheet = workbook.add_worksheet()

xlsx_row = 0
xlsx_column = 0

#Результат хэширования
worksheet.write(xlsx_row, xlsx_column, 'Хэшированный ip-адрес:')
xlsx_row += 1
for ip in ip_sha256:
    worksheet.write(xlsx_row, xlsx_column, ip)
    xlsx_row += 1

xlsx_column = 0
xlsx_row += 2

#Результат определения страны, города, координат по ip-адресу
worksheet.write(xlsx_row, xlsx_column, 'Страна, город, координаты по ip-адресу:')
xlsx_row += 1
xlsx_row_buf = 0

worksheet.write(xlsx_row, xlsx_column, 'Страна')
worksheet.write(xlsx_row+1, xlsx_column, 'Город')
worksheet.write(xlsx_row+2, xlsx_column, 'Координаты')
xlsx_column += 1

for row in country_city_loc_by_ip:
    for elem in row:
        worksheet.write(xlsx_row + xlsx_row_buf, xlsx_column, elem)
        xlsx_row_buf+= 1
    xlsx_column += 1
    xlsx_row_buf = 0
    

xlsx_column = 0
xlsx_row += len(country_city_loc_by_ip[0]) + 2

#Результат определения страны, города по координатам
worksheet.write(xlsx_row, xlsx_column, 'Страна, город по координатам:')
xlsx_row += 1
xlsx_row_buf = 0

worksheet.write(xlsx_row, xlsx_column, 'Страна')
worksheet.write(xlsx_row+1, xlsx_column, 'Город')
xlsx_column += 1

for row in country_city_by_coords:
    for elem in row:
        worksheet.write(xlsx_row + xlsx_row_buf, xlsx_column, elem)
        xlsx_row_buf+= 1
    xlsx_column += 1
    xlsx_row_buf = 0
    

xlsx_column = 0
xlsx_row += len(country_city_by_coords[0]) + 2

#Результат определения координат по городу
worksheet.write(xlsx_row, xlsx_column, 'Координаты по городу:')
xlsx_row += 1
xlsx_row_buf = 0

worksheet.write(xlsx_row, xlsx_column, 'Lat')
worksheet.write(xlsx_row+1, xlsx_column, 'Lon')
xlsx_column += 1

for row in coords_by_city:
    for elem in row:
        worksheet.write(xlsx_row + xlsx_row_buf, xlsx_column, elem)
        xlsx_row_buf+= 1
    xlsx_column += 1
    xlsx_row_buf = 0
    
workbook.close()